const mongodb = require('mongodb');
const getDb= require('../helper/database').getDb;
const {roles} = require('../middleware/constants');


class User{
    constructor(username,password,name){
        this.username = username;  
        this.password = password;
        this.name = name;
        this.role = roles.user;
    };

    save(){
        const db = getDb();
        db.collection('users').insertOne(this);
    };

   
}

module.exports = User;