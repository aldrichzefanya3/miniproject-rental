const mongodb = require('mongodb');
const getDb= require('../helper/database').getDb;

class Genres{
    constructor(id,code,genreName){
        this._id = id ? new mongodb.ObjectId(id) : null
        this.code = code
        this.genreName = genreName;
    }

    save(){
        const db = getDb();
        let dbOp = db.collection('genres').insertOne(this);
        return dbOp
        .then(result=>{
            console.log(result)
        }).catch(err => console.log(err));
    }
    
    static find(){
        const db = getDb();
        let dbOp = db.collection('genres').find({}).toArray();
        return dbOp 
    }

    static findByName(genreName){
        var arrCode = [];
        const db = getDb();
        for(let i=0;i<genreName.length;i++){
            var gen = db.collection('genres').aggregate([{ $match: {genreName: genreName[i]}}])
            .next()
            .then(genre =>{
                arrCode.push(genre.code);
                return arrCode;
            }).catch(err => console.log(err))
        }
        return gen; 
    };

    static async findByCodes(produk){
        let arrCode = [];
        const genre = produk.genres;
        const db =getDb();
        for(let i=0;i<genre.length;i++){
            var genres = await db.collection('genres').aggregate([{ $match: {code: genre[i]}}])
            .next()
            .then(genre =>{
                arrCode.push(genre.genreName);
                return arrCode;
            }).catch(err => console.log(err))
        } 
        return genres; 
    }
    
}

module.exports = Genres;