const mongodb = require('mongodb');
const getDb= require('../helper/database').getDb;
const Genres = require('../models/genres')
const Author = require('../models/author');

class Product{
    constructor(title,author_id,genreCode,publishedDate, rating,numberOfPage,publishedPlace,status,id){
        this.title = title;
        this.author_id = new mongodb.ObjectId(author_id)
        this.genres = genreCode; 
        this.publishedDate = publishedDate;
        this.rating = rating;
        this.numberOfPage = numberOfPage;
        this.publishedPlace = publishedPlace;
        this.status = status;
        this._id = id ? new mongodb.ObjectId(id) : null;
    }

    save(){
        const db = getDb();
        let dbOp;//dboperation
        if(this._id){
            //update the product
            dbOp = db.collection('products').updateOne({_id:this._id}, {$set: this });
        }else{
            dbOp = db.collection('products').insertOne(this);
        }
        return dbOp
        .then( result =>{
            console.log(result);
        })
        .catch( err =>{
            console.log(err);
        });
    }

    static updatedStatus(book_id){
        const db = getDb();
        let dbOp;
        dbOp = db.collection('products').updateOne({_id:book_id}, {$set: this });
        
        return dbOp
    }

    static async find(){
        const db = getDb();
        const products = await db.collection('products').find().toArray()
        for(let produk of products){
            var author = await Author.findById(produk);
            var genreName = await Genres.findByCodes(produk);
            produk.genres = genreName;  
            produk.author_id = author;  
        }  
        return products;
    }


    static async filterArray(genres){
        const db =getDb()
        return db.collection('products').find({genres:{$in:[genres]}}).toArray()
        .then(items =>{
            return items
        })
    }

    static async filterSearch(){
        const db = getDb();
        const filteredProducts = await db.collection('products').find().toArray()
        for(let produk of filteredProducts){
                var author = await Author.findById(produk);
                var genreName = await Genres.findByCodes(produk);
                produk.genres = genreName;  
                produk.author_id = author;  
        }
        return filteredProducts 
    }

    static deleteById(bookId){
        const db = getDb();
        return db.collection('products').deleteOne({_id: new mongodb.ObjectId(bookId)})
    }
}

module.exports = Product;