const mongodb = require('mongodb');
const getDb= require('../helper/database').getDb;

class RentedBooks{
    constructor(book_id,user_id,startDate,endDate){
        this.book_id = new mongodb.ObjectId(book_id);
        this.user_id = new mongodb.ObjectId(user_id);
        this.startDate = startDate;
        this.endDate = endDate;
    }
    
    save(){
        const db = getDb();
        return db.collection('rentedBooks').insertOne(this);
    }
    
    static getOrders(userId){
        const db = getDb();
        return db.collection('rentedBooks').find({user_id: new mongodb.ObjectId(userId)}).toArray()
    }
    

}

module.exports = RentedBooks;