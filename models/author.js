const mongodb = require('mongodb');
const getDb= require('../helper/database').getDb;

class Author{
    constructor(id,firstName,lastName, nationality){
        {this.firstName = firstName;
        this.lastName = lastName;
        this.nationality = nationality;
        this._id = id ? new mongodb.ObjectId(id) : null;}
    }

    save(){
        const db = getDb();
        let dbOp = db.collection('author').insertOne(this);
        return dbOp
    };
    
    static find(){
        const db = getDb();
        let dbOp = db.collection('author').find({}).toArray();
        return dbOp 
    }

    static findByName(name){
        const db = getDb();
        var author = db.collection('author')
        .aggregate([{$match: { firstName: name.split(' ')[0], lastName: name.split(' ')[1]}}])
        .next()
        .then(author =>{
            let authorId = author._id;
            return authorId;
        })
        .catch(err => console.log(err))
        return author;
    };

    static findById(produk){
        const db = getDb();
        const author = produk.author_id;
        return db.collection('author').aggregate([{$match:{_id: author}}]).next().then(docAuthor =>{
            let firstName = docAuthor.firstName;
            let lastName = docAuthor.lastName;
            let fullName = firstName.concat(" ",lastName);

            return fullName;
        }).catch(err => console.log(err));
    }
}

module.exports = Author;
