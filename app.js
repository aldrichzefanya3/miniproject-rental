const express = require('express')
const MONGODB_URI   = 'mongodb+srv://aldrich:XxYfZs1Hn9pXHYOW@rental01.ljh9g.mongodb.net/rental?'


const mongoConnect = require('./helper/database').mongoConnect
const app = express()

const adminRoutes = require('./routes/admin')
const authRoutes = require('./routes/auth')
const userRoutes = require('./routes/user')

app.use(express.urlencoded({extended: true}))
app.use(express.json())

app.use('/api/admin', adminRoutes)
app.use('/api/auth', authRoutes)
app.use('/api/user', userRoutes)

mongoConnect(()=>{
    app.listen(3000)
});