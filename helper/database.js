const mongodb = require('mongodb');
const MongoClient = mongodb.MongoClient;

let db;

const mongoConnect = (callback) => {
    MongoClient.connect('mongodb+srv://aldrich:XxYfZs1Hn9pXHYOW@rental01.ljh9g.mongodb.net/rental?retryWrites=true&w=majority')
    .then(result => {
        console.log("Connected");
        db = result.db();//store a connection to db variable
        callback();
    })
};

const getDb = () =>{
    if(db){
        return db;
    }
    throw 'No database found';
};

module.exports ={
    mongoConnect,
    getDb
};
