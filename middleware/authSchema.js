const Joi = require('joi');


const authSchema = Joi.object({
    username: Joi.string().alphanum().min(5).max(20).required().messages({'string.min':`"username" harus memiliki minimum 5 karakter`}),
    password: Joi.string().alphanum().min(8).required().messages({'string.required':`"password" harus diisi`,'string.min': `"password" minimal 8 karakter terdiri dari huruf atau angka`}),
    confirmPassword: Joi.ref('password'),
    name: Joi.string().required(),
    // role: Joi.string().empty('').default('USER')
})  

module.exports = {
    authSchema
}