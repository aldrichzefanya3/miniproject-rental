const {roles} = require('../middleware/constants');
const getDb= require('../helper/database').getDb;


module.exports = async(req,res,next)=>{
    const db = getDb()
    const {username} = req.body;
    const activeUser = await db.collection('users').aggregate([{ $match: { username: username }}])
    .next()
    .then(result =>{
        if(result.role === roles.admin){
            next()
        }else{
            return res.status(500).json({
                message: "Hanya dapat diakses oleh ADMIN"
            })
        }
        return result;
    })
 
}