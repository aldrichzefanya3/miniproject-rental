const express = require('express')

const authController = require('../controllers/auth')

const router = express.Router()

router.post('/register', authController.create_an_user)
router.post('/login', authController.login_user)

module.exports = router;