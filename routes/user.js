const express = require('express')

const bookController = require('../controllers/book')
const rentedBookController = require('../controllers/rentedBooks')

const router = express.Router();

const isAuth = require("../middleware/is-Auth")
const isUser = require('../middleware/is-user')

//read-book
router.get('/list-book', isAuth, isUser, bookController.get_all_books)
router.post('/rent-book', isAuth, isUser, rentedBookController.rent_a_book)
router.get('/list-book/search',isAuth, isUser, bookController.filter_n_search)

module.exports = router;