const express = require('express');


const authorController = require('../controllers/author')
const genreController = require('../controllers/genre')
const bookController = require('../controllers/book')

const router = express.Router();

const isAuth = require("../middleware/is-Auth")
const isAdmin = require('../middleware/is-admin')



//admin/add-author 
router.post('/add-author',isAuth, isAdmin, authorController.create_an_author)
router.get('/author',isAuth, isAdmin, authorController.get_all_author)

//admin/add-genre
router.post('/add-genre',isAuth, isAdmin, genreController.create_genre)
router.get('/genre',isAuth, isAdmin, genreController.get_all_genres)

//admin/crud-book
router.post('/add-book',isAuth, isAdmin, bookController.create_a_book)
router.put('/edit-book/:id',isAuth, isAdmin, bookController.edit_a_book)
router.delete('/delete-book/:id',isAuth, isAdmin, bookController.delete_a_book)
router.get('/list-book', isAuth, isAdmin, bookController.get_all_books)



module.exports = router;