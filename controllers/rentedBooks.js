const RentedBooks = require('../models/rentedBooks')
const Product = require('../models/product');
const getDb= require('../helper/database').getDb;



const rent_a_book = async(req,res,next)=>{
    const {book_id} = req.query
    const {user_id,startDate,endDate,status} = req.body
    const rentedBook =  new RentedBooks(book_id,user_id,startDate,endDate)
    try {
        const docRentedBook = await rentedBook.save().then(result =>{
                res.status(200).json({
                    message: "Berhasil meminjam buku",
                    createdRentedBook: {
                        request: {
                            type: 'POST'
                        }
                    }
                })
            })
            const db = getDb()
            const rentedBooks = db.collection('rentedBooks')
            .aggregate([{ $lookup: {
                from: "products",
                localField: "book_id",
                foreignField: "_id",
                as: "book_detail"
            }}]).next().then(result =>{
                const status =  "tidak tersedia"
                const title =  result.book_detail[0].title
                const publishedDate = result.book_detail[0].publishedDate
                const publishedPlace = result.book_detail[0].publishedPlace
                const rating = result.book_detail[0].rating
                const genreName = result.book_detail[0].genres
                const numberOfPage = result.book_detail[0].numberOfPage
                const author = result.book_detail[0].author_id
                const _id = book_id
                const updatedStat = new Product(title,author,genreName,publishedDate,publishedPlace,rating,numberOfPage,status,_id)
                const results =  updatedStat.save()    
            })
    } catch (error) {
        console.log(error)
    }
}


module.exports = {
    rent_a_book
}