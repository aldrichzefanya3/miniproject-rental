const Author = require('../models/author');


const get_all_author = async(req,res,next)=>{
    try{
        const docsAuthor = await Author.find()
        .then(results => {
            const response ={
                author: results.map( 
                    result=>{
                    return {
                        firstName: result.firstName,
                        lastName: result.lastName,
                        nationality: result.nationality,
                        _id: result._id,
                        request:{
                            type: 'GET'
                        }
                    }
                })
            }
        res.status(200).json(response)
    })
    }catch(err){ console.log(err) }
  
}

const create_an_author = async(req,res,next) =>{
    const firstName = req.body.firstName;
    const lastName = req.body.lastName;
    const nationality = req.body.nationality;
    const author = new Author(null,firstName,lastName,nationality)
    try {
        const docAuthor = await author.save().then(result =>{
            res.status(200).json({
                message: "Input Author sukses",
                createdAuthor: {
                    request: {
                        type: 'POST'
                    }
                }
            })
          
        })
    } catch (error) {
        console.log(error)
    }
}


module.exports = {
   create_an_author,
   get_all_author
};