const User = require('../models/user');
const getDb= require('../helper/database').getDb;
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const {authSchema} = require('../middleware/authSchema');
const config  = require('../config')

const create_an_user = (req,res,next) =>{
    const username = req.body.username;
    const password = req.body.password;
    const name = req.body.name;
    const db = getDb();
    
    const validation = authSchema.validate({username,password,name})
    if(validation.error){
        return res.status(400).json({
            message: validation.error
        })
    }
    db.collection('users').findOne({username: username})
        .then(userDoc => {
            if(userDoc){
                return res.status(500).json({
                    message: "Username sudah dipakai",
                    createdUser: {
                        request: {
                            type: 'POST'
                        }
                    }
                })
            }
            return bcrypt.hash(password,12).then(hashedPassword =>{
                const user = new User(
                    username, 
                    hashedPassword,
                    name 
                );
                return user.save();
            }).then(result =>{
                res.status(200).json({
                    message: "Berhasil Registerasi",
                    createdUser: {
                        request: {
                            type: 'POST'
                        }
                    }
                })
            });
        })
        .catch(err =>{ 
            console.log(err);
        });
    
    
};


const login_user = async(req,res,next) =>{
    const username = req.body.username;
    const password = req.body.password;
    const db = getDb();
    try{
        const user = await db.collection('users').findOne({username: username})
        if(!user){
            return res.status(500).json({
                message: "Username tidak ada",
                loginUser: {
                    request: {
                        type: 'POST'
                    }
                }
            })
        }
        const doMatch = await bcrypt.compare(password, user.password)
        if(doMatch){
            const token = jwt.sign({
                username: user.username,
                userID: user._id
                },
                config.env,
                {
                    expiresIn: '1h'
                }
            );
            return res.status(200).json({
                message: "Login berhasil",
                token: token,
                loginUser: {
                    request: {
                        type: 'POST'
                    }
                }
            })     
        }
        return res.status(500).json({
            message: "Password atau Username salah",
            loginUser: {
                request: {
                    type: 'POST'
                }
            }
        })
    }catch(err){
        return res.status(500).json({
            message: "ERROR"
        })
    }
};


module.exports = {
    create_an_user,
    login_user
};