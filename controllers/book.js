const Product = require('../models/product');
const Author = require('../models/author');
const Genres = require('../models/genres')

//create-a-book
const create_a_book = async (req,res,next)=>{
    try{
        const title = req.body.title;
        const publishedDate = req.body.publishedDate;
        const rating = req.body.rating;
        const numberOfPage = req.body.numberOfPage;
        const publishedPlace = req.body.publishedPlace;
        const name = req.body.name;
        const status = req.body.status;
        const genreName= req.body.genreName; 
        if(typeof(genreName) === 'string'){
            const genreName= [req.body.genreName]; 
            const genreCode = await Genres.findByName(genreName);
            const _id = await Author.findByName(name)
            const product = await new Product(title,_id,genreCode,publishedDate,rating,numberOfPage,publishedPlace,status);
            const result =  await product
                            .save()
                            .then(book =>{
                                res.status(200).json({
                                    message: "Input Buku sukses",
                                    request: {
                                        type: 'POST'
                                    }
                                })
                            })
         
        }else{
            const genreCode = await Genres.findByName(genreName);
            const _id = await Author.findByName(name)
            const product = await new Product(title,_id,genreCode,publishedDate,rating,numberOfPage,publishedPlace,status);
            const result =  await product
                            .save()
                            .then(book =>{
                                res.status(200).json({
                                    message: "Input Buku sukses",
                                    request: {
                                        type: 'POST'
                                    }
                                })
                            })
        }
    }catch(error){
        console.log(error);
    }        
};


//edit-book
const edit_a_book = async (req,res,next) =>{
    try{
        const bookId = req.params.id;
        const updatedTitle = req.body.title;
        const updatedPublishedDate = req.body.publishedDate;
        const updatedPublishedPlace = req.body.publishedPlace;
        const updatedRating = req.body.rating;
        const updatedNumberOfPage = req.body.numberOfPage;
        const name = req.body.name;
        const genreName = req.body.genreName;
        const status = req.body.status;
        if(typeof(genreName) === 'string'){
            const genreName = [req.body.genreName];
            const docGenre = await Genres.findByName(genreName)
            const updatedGenreCode = docGenre;
            const docAuthor = await Author.findByName(name)
            const updatedAuthor_id = docAuthor;   
            const product = await new Product(
                updatedTitle,
                updatedAuthor_id,
                updatedGenreCode,
                updatedPublishedDate,
                updatedRating,
                updatedNumberOfPage,
                updatedPublishedPlace,
                status,
                bookId
            );
            const result =  await product
            .save()
            .then(book =>{
                res.status(200).json({
                    message: "Update Informasi Buku sukses",
                    request: {
                        type: 'POST'
                    }
                })
            })
        }else{
            const docGenre = await Genres.findByName(genreName)
            const updatedGenreCode = docGenre;
            const docAuthor = await Author.findByName(name)
            const updatedAuthor_id = docAuthor;   
            const product = await new Product(
                updatedTitle,
                updatedAuthor_id,
                updatedGenreCode,
                updatedPublishedDate,
                updatedRating,
                updatedNumberOfPage,
                updatedPublishedPlace,
                status,
                bookId
            );
            const result =  await product
            .save()
            .then(book =>{
                res.status(200).json({
                    message: "Update Informasi Buku sukses",
                    request: {
                        type: 'PUT'
                    }
                })
            })
        }
    }catch(err) {console.log(err)};
};

//get-a-books
const get_all_books = async(req,res,next)=>{
    try{
        const docsBooks = await Product.find()
        .then(results => {    
            const response ={
                book: results.map( 
                    result=>{
                    return {
                        title: result.title,
                        author: result.author_id,
                        genres: result.genres,
                        rating: result.rating,
                        publishedDate: result.publishedDate,
                        publishedPlace: result.publishedPlace,
                        numberOfPage: result.numberOfPage,
                        status: result.status,
                        request:{
                            type: 'GET'
                        }
                    }
                })
            }
        res.status(200).json(response)
    })
    }catch(err){ console.log(err) }
  
}

const delete_a_book = async (req,res,next) => {
    const bookId = req.params.id;
    const result = await Product.deleteById(bookId)
                   .then(result =>{
                       res.status(200).json({
                        message: "Hapus Buku sukses",
                        request: {
                            type: 'DELETE'
                        }
                       })
                   }) 
};


//filter n search
const filter_n_search = async (req,res,next)=>{
    const filters = req.query
    const {genres} = req.query;
    if(genres){
        const filter = await Product.filterArray(genres).then(results =>{
            console.log(results);
        })
        return filter
    }else{
        try {
            const arr = []
            const docsBooks = await Product.filterSearch().then(results =>{
                results.filter(result=>{ 
                    let isValid = true
                    for (let key in filters) {
                        isValid = isValid && result[key] == filters[key];    
                    }
                    if(isValid === true){
                        arr.push(result)
                        return result;
                    }
                })
                const response ={
                    resultFilter: arr.map( 
                        result=>{
                        return {
                            result: result,
                            request:{
                                type: 'GET'
                            }
                        }
                    })
                }
                res.status(200).json(response)
                return results
            })
          
        } catch (error) {
            console.log(error)
        }
    }
   
    
}

module.exports = {
    create_a_book,
    edit_a_book,
    get_all_books,
    delete_a_book,
    filter_n_search
};
