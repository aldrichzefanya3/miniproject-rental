const Genres = require('../models/genres');

const get_all_genres = async(req,res,next)=>{
    try{
        const docsGenre = await Genres.find()
        .then(results => {
            const response ={
                genre: results.map( 
                    result=>{
                    return {
                        code: result.code,
                        genreName: result.genreName,
                        _id: result._id,
                        request:{
                            type: 'GET'
                        }
                    }
                })
            }
        res.status(200).json(response)
    })
    }catch(err){ console.log(err) }
  
}
const create_genre = async(req,res,next) =>{
    const code = req.body.code;
    const genreName = req.body.genreName;
    const genre = new Genres(null,code,genreName)
    try {
        const docGenre = await genre.save().then(result =>{
            res.status(200).json({
                message: "Input Genre sukses",
                createdAuthor: {
                    request: {
                        type: 'POST'
                    }
                }
            })
          
        })
    } catch (error) {
        console.log(error)
    }
};

module.exports = {
    create_genre,
    get_all_genres
};